//
//  ViewController.swift
//  Northwest-KCProgrammingContestApp
//
//  Created by Pannala,Vishal Reddy on 3/13/19.
//  Copyright © 2019 Pannala,Vishal Reddy. All rights reserved.
//

import UIKit

class NewSchoolController: UIViewController {

    @IBOutlet weak var coachNameTF: UITextField!
    @IBOutlet weak var schoolNameTF: UITextField!
    
    var newSchool: School!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func done(_ sender: Any) {
        let schoolName = schoolNameTF.text!
        let coachName = coachNameTF.text!
        if schoolNameTF.text!.count != 0 && coachNameTF.text!.count != 0{
//        Schools.shared.add(school: School(name: schoolName, coach: coachName, teams: []))
        Schools.shared.saveSchool(name: schoolName, coach: coachName)
        self.dismiss(animated: true, completion: nil)
        }else if schoolNameTF.text!.count == 0 {
            displayMessage(fieldName: "Name")
        }else if coachNameTF.text!.count == 0 {
            displayMessage(fieldName: "Coach")
        }
    }

    func displayMessage(fieldName: String){
        let alert = UIAlertController(title: "Note",
                                      message: "Please enter a value in \(fieldName) text field.",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

